$(document).ready(function () {
    refresh();
});

//hendlanje link button-a
//-------------------------------------------------------------
$("#loginBtn").click(function () {
    $("#container").html(loginForm);
});

$("#logoutBtn").click(function () {
    logout();
});

$("#pregledBtn").click(function () {
    showKorisnici();
});
//-------------------------------------------------------------

//kontrole za unos podataka
//------------------------LOGIN--------------------------------
$(document).on('click', '#getLogin', function () {
    var email = $('#inputEmail').val();
    var password = $('#inputPassword').val();
    if (email == null || email == "") {
        Swal.fire('Molimo unesite email adresu');
    } else if (password == null || password == "") {
        Swal.fire('Molimo unesite zaporku');
    } else {
        login();
    }
})

//-----------------------SAVE KORISNIK---------------------------
$(document).on('click', '#spremiKor', function () {
    var ime = $('#ime').val();
    var prezime = $('#prezime').val();
    var email = $('#email').val();
    var OIB = $('#OIB').val();
    var OVLASTI = $('#OVLASTI').val();
    var GODISTE = $('#GODISTE').val();
    var SPOL = $('#SPOL').val();
    var ID = $('#ID').val();
    console.log("ID=" + ID);

    if (ime == null || ime == "") {
        Swal.fire('Molimo unesite ime korisnika');
    } else if (prezime == null || prezime == "") {
        Swal.fire('Molimo unesite prezime korisnika');
    } else if (email == null || email == "") {
        Swal.fire('Molimo unesite email korisnika');    
    } else if (OIB == null || OIB == "") {
        Swal.fire('Molimo unesite OIB korisnika');    
    } else if (OVLASTI == null || OVLASTI == "") {
        Swal.fire('Molimo unesite ovlasti korisnika'); 
    } else if (SPOL == null || SPOL == "") {
        Swal.fire('Molimo unesite spol korisnika');        
    } else if (GODISTE == null || GODISTE == "") {
        Swal.fire('Molimo unesite godište korisnika');    
    } else {
        var url = "https://dev.vub.zone/sandbox/router.php"
        $.ajax({
            type: 'POST',
            url: url,
            data: {"projekt": "p_jerkovic", "procedura": "p_save", "ID":ID, "ime": ime, "prezime": prezime, "email": email, "OIB": OIB, "OVLASTI": OVLASTI, "SPOL": SPOL, "GODISTE": GODISTE},
            success: function (data) {
                var jsonBody = JSON.parse(data);
                var errcode = jsonBody.h_errcode;
                var message = jsonBody.h_message;
                console.log(message+errcode);

                if ((message == null || message == "") && (errcode == null || errcode == 0)) {
                    Swal.fire('Uspješno se unijeli korisnika');
                } else {
                    Swal.fire(message + '.' + errcode);
                }
                refresh();
                showKorisnici();
            },
            error: function (xhr, textStatus, error) {
                console.log(xhr.statusText);
                console.log(textStatus);
                console.log(error);
            },
            async: true
        });
    }
})
//-------------------------------------------------------------


$.ajaxSetup({
    xhrFields: {
        withCredentials: true
    }
});

function login() {
    var url = "https://dev.vub.zone/sandbox/router.php"
    $.ajax({
        type: 'POST',
        url: url,
        data: {"projekt": "p_common", "procedura": "p_login", "username": $('#inputEmail').val(), "password": $('#inputPassword').val()},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcod = jsonBody.h_errcod;
            var message = jsonBody.h_message;

            if (message == null || message == "", errcod == null || errcod == 0) {
                $("#container").html('');
            } else {
                Swal.fire(message + '.' + errcod);
            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

function refresh() {
    var url = "https://dev.vub.zone/sandbox/router.php"
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_common", "procedura": "p_refresh" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var podaci = '<small>ID:' + jsonBody.ID + '<br>' + 'ime prezime:' + jsonBody.ime + ' ' + jsonBody.prezime + '<br>' + 'email:' + jsonBody.email + '<br>' + 'godiste:' + jsonBody.godiste + '</small>';
            $("#podaci").html(podaci);
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

function logout() {
    var url = "https://dev.vub.zone/sandbox/router.php"
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_common", "procedura": "p_logout" },
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;

            if (message == null || message == "" || errcode == null) {
                Swal.fire("Greška u obradi podataka, molimo pokušajte ponovno!");
            } else {
                Swal.fire(message + '.' + errcode);
            }
            refresh();
            $("#container").html('');
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true
    });
}

function showKorisnici(page) {
    var url = "https://dev.vub.zone/sandbox/router.php"
    var tablica = '<br><button type="button" style="float:right;" class="btn btn-success" onclick="insertForm('+ page + ')">Insert <i class="fa fa-download" aria-hidden="true"></i></button><br><br>';
    tablica += '<table class="table table-hover"><tbody><thead><tr><th scope="col">ime</th><th scope="col">prezime</th><th scope="col">email</th><th scope="col">OIB</th><th scope="col">action</th></tr>';
    var perPage = 10;
    
    if (page == null || page == "") {
        page = 1;
    }    
        
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_jerkovic", "procedura": "p_get", "perPage": perPage, "page": page},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            var count   = jsonBody.count;


            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    tablica += '<tr><td>' + v.ime + '</td>';
                    tablica += '<td>' + v.prezime + '</td>';
                    tablica += '<td>' + v.email + '</td>';
                    tablica += '<td>' + v.OIB + '</td>';
                    tablica += '<td><button type="button" class="btn btn-primary" onclick="showKorisnik('+ v.ID + ',' + page + ')">Edit <i class="fas fa-edit"></i></button> ';  
                    tablica += '<button type="button" class="btn btn-danger" onclick="delKorisnik('+ v.ID + ',' + page + ')">Delete <i class="far fa-trash-alt"></i></button></td></tr>';
                });
                tablica += '</tbody></table>';
                tablica += pagination(page, perPage, count);
                $("#container").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}


function showKorisnik(ID, page) {
    var url = "https://dev.vub.zone/sandbox/router.php"
    var tablica = '<table class="table table-hover"><tbody>';        
    $.ajax({
        type: 'POST',
        url: url,
        data: { "projekt": "p_jerkovic", "procedura": "p_get", "ID": ID},
        success: function (data) {
            var jsonBody = JSON.parse(data);
            var errcode = jsonBody.h_errcode;
            var message = jsonBody.h_message;
            
            if (message == null || message == "", errcode == null || errcode == 0) {
                $.each(jsonBody.data, function (k, v) {
                    tablica += '<tr><th scope="col">ID</th><td><input type="text" id="ID" value="' + v.ID + '" readonly></td></tr>';
                    tablica += '<tr><th scope="col">ime</th><td><input type="text" id="ime" value="' + v.ime + '"></td></tr>';
                    tablica += '<tr><th scope="col">prezime</th><td><input type="text" id="prezime" value="' + v.prezime + '"></td></tr>';
                    tablica += '<tr><th scope="col">email</th><td><input type="text" id="email" value="' + v.email + '"></td></tr>';
                    tablica += '<tr><th scope="col">OIB</th><td><input type="text" id="OIB" value="' + v.OIB + '"></td></tr>';
                    tablica += '<tr><th scope="col">Ovlasti</th><td><input type="text" id="OVLASTI" value="' + v.OVLASTI + '"></td></tr>';
                    tablica += '<tr><th scope="col">Spol</th><td><input type="text" id="SPOL" value="' + v.SPOL + '"></td></tr>';
                    tablica += '<tr><th scope="col">godiste</th><td><input type="text" id="GODISTE" value="' + v.GODISTE + '"></td></tr></table>';
                    tablica += '<button type="button" class="btn btn-warning" id="spremiKor">Spremi <i class="fas fa-save"></i></button> ';
                    tablica += '<button type="button" class="btn btn-success" onclick="showKorisnici('+ page +')">Odustani <i class="fas fa-window-close"></i></button>';
                });
                $("#container").html(tablica);
            } else {
                if (errcode == 999) {
                    $("#container").html(loginForm);
                } else {
                    Swal.fire(message + '.' + errcode);
                }
            }
            refresh();
        },
        error: function (xhr, textStatus, error) {
            console.log(xhr.statusText);
            console.log(textStatus);
            console.log(error);
        },
        async: true

    });
}

function loginForm() {
    var output = "<br><br><form><div class='form-group'>";
    output += "<label for='inputEmail'>Email adresa</label>";
    output += "<input type='email' class='form-control' id='inputEmail' aria-describedby='emailHelp' placeholder='Enter email'></div>";
    output += "<div class='form-group'><label for='inputPassword'>Password</label>";
    output += "<input type='password' class='form-control' id='inputPassword' placeholder='Password'></div>";
    output += "<button class='btn btn-primary' id='getLogin'>Submit</button></form>";
    return output;
}


function insertForm(page) {
    var output = '<table class="table table-hover"><tbody>'; 
    output += '<tr><th scope="col">ime</th><td><input type="text" id="ime"></td></tr>';
    output += '<tr><th scope="col">prezime</th><td><input type="text" id="prezime"></td></tr>';
    output += '<tr><th scope="col">email</th><td><input type="text" id="email"></td></tr>';
    output += '<tr><th scope="col">OIB</th><td><input type="text" id="OIB"></td></tr>';
    output += '<tr><th scope="col">Ovlasti</th><td><input type="text" id="OVLASTI"></td></tr>';
    output += '<tr><th scope="col">Spol</th><td><input type="text" id="SPOL"></td></tr>';
    output += '<tr><th scope="col">godiste</th><td><input type="text" id="GODISTE"></td></tr></table>';
    output += '<button type="button" class="btn btn-warning" id="spremiKor">Spremi <i class="fas fa-save"></i></button> ';
    output += '<button type="button" class="btn btn-success" onclick="showKorisnici('+ page +')">Odustani <i class="fas fa-window-close"></i></button>';
    $("#container").html(output);
}

function pagination(pageNmb, perPage, count) {
    //ne treba prikazivati ništa
    if (count < perPage){
       return '';
    }else{
        var quotient = Math.ceil(count/perPage);
    }
    var next = pageNmb + 1; 
    var prev = pageNmb - 1;    
    var pagination = '<div class="float-right pagination">';
    
    //treba prikazati previous
    if (pageNmb > 1){
        pagination += '<ul class="pagination"><li class="page-item "><a class="page-link" onclick="showKorisnici('+ prev + ')" href="javascript:void(0)">‹</a></li>';
    }

    for (i = pageNmb; i < pageNmb+8; i++) {
        pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici('+ i + ')" href="javascript:void(0)">'+ i +'</a></li>';    
    }
 
    pagination += '<li class="page-item"><a class="page-link"  href="javascript:void(0)">...</a></li>';
    
    pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici('+ quotient + ')" href="javascript:void(0)">'+ quotient +'</a></li>';    
        
    pagination += '<li class="page-item"><a class="page-link" onclick="showKorisnici('+ next + ')" href="javascript:void(0)">›</a></li>';
    pagination += '</ul></div>';
    return pagination;
}
